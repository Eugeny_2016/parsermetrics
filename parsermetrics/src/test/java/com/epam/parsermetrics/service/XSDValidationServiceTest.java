package com.epam.parsermetrics.service;

import org.junit.Assert;
import org.junit.Test;

/**
 * The Class for testing methods of XSDValidationService class
 */
public class XSDValidationServiceTest {
	
	/**
	 * Tests validating xml file against its xsd schema
	 */
	@Test
    public void validateByXSD() {
		boolean actualResult = XSDValidationService.validateByXSD("xml/tariffs.xml", "xml/tariffs.xsd");
		Assert.assertEquals(true, actualResult);
		
		actualResult = XSDValidationService.validateByXSD("xml/bad_tariffs.xml", "xml/tariffs.xsd");
		Assert.assertEquals(false, actualResult);
    }
}

package com.epam.parsermetrics.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.epam.parsermetrics.entity.CallParameterName;
import com.epam.parsermetrics.entity.InternetParameterName;
import com.epam.parsermetrics.entity.TariffName;

/**
 * The Class for testing methods of AddService class
 */
public class ValidationServiceTest {
	
	/**
	 * Tests call tariff validation
	 */
	@Test
    public void validateCallTariff() {
		Map<CallParameterName, Object> nativePriceTable = new HashMap<>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<>();
		Map<CallParameterName, Object> otherPriceTable = new HashMap<>();
		
		String result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals(null, result);
		
		result = ValidationService.validate("", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect tariff id", result);
		
		result = ValidationService.validate("t2012_no.1", "MTSS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect operator name", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", null, 54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect tariff name", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, -54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect connection fee", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, -30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect initial payment", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, -7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect internet speed default", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, -251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect sms price", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, -750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect mms price", result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, -2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals("incorrect every megabyte price", result);
    }
	
	/**
	 * Tests Internet tariff validation
	 */
	@Test
    public void validateInternetTariff() {
		Map<CallParameterName, Object> nativePriceTable = new HashMap<>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<>();
		Map<CallParameterName, Object> otherPriceTable = new HashMap<>();
		Map<InternetParameterName, Object> internetPriceTable = new HashMap<>();
		
		String result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 15, 
				nativePriceTable, stationaryPriceTable, otherPriceTable, internetPriceTable);
		Assert.assertEquals(null, result);
		
		result = ValidationService.validate("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, -15, 
				nativePriceTable, stationaryPriceTable, otherPriceTable, internetPriceTable);
		Assert.assertEquals("incorrect internet speed max", result);
    }
}

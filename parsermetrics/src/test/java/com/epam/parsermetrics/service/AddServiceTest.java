package com.epam.parsermetrics.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.epam.parsermetrics.entity.CallParameterName;
import com.epam.parsermetrics.entity.CallTariff;
import com.epam.parsermetrics.entity.InternetParameterName;
import com.epam.parsermetrics.entity.InternetTariff;
import com.epam.parsermetrics.entity.Tariff;
import com.epam.parsermetrics.entity.TariffName;

/**
 * The Class for testing methods of AddService class
 */
public class AddServiceTest {
	
	/**
	 * Tests adding call tariff
	 */
	@Test
    public void addCallTariff() {
		Set<Tariff> tariffSetExpected = new HashSet<>();
		Set<Tariff> tariffSetActual = new HashSet<>();
		Map<CallParameterName, Object> nativePriceTable = new HashMap<>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<>();
		Map<CallParameterName, Object> otherPriceTable = new HashMap<>();
		Tariff tariff = new CallTariff("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
										nativePriceTable, stationaryPriceTable, otherPriceTable);
		tariffSetExpected.add(tariff);	
		boolean result = AddService.addTariff(tariffSetActual, "t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals(true, result);
		Assert.assertEquals(tariffSetExpected, tariffSetActual);
    }
	
	/**
	 * Tests adding Internet tariff
	 */
	@Test
    public void addInternetTariff() {
		Set<Tariff> tariffSetExpected = new HashSet<>();
		Set<Tariff> tariffSetActual = new HashSet<>();
		Map<CallParameterName, Object> nativePriceTable = new HashMap<>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<>();
		Map<CallParameterName, Object> otherPriceTable = new HashMap<>();
		Map<InternetParameterName, Object> inernetPriceTable = new HashMap<>();
		Tariff tariff = new InternetTariff("t2014_no.3", "MTS", TariffName.SMART_PLUS, 54900, 30000, 7, 251, 750, 15, 
										nativePriceTable, stationaryPriceTable, otherPriceTable, inernetPriceTable);
		tariffSetExpected.add(tariff);	
		boolean result = AddService.addTariff(tariffSetActual, "t2014_no.3", "MTS", TariffName.SMART_PLUS, 54900, 30000, 7, 251, 750, 15, 
				nativePriceTable, stationaryPriceTable, otherPriceTable, inernetPriceTable);
		Assert.assertEquals(true, result);
		Assert.assertEquals(tariffSetExpected, tariffSetActual);
    }
	
	/**
	 * Tests adding a denied call tariff
	 */
	@Test
    public void addDeniedCallTariff() {
		Map<Tariff, String> deniedTariffsExpected = new HashMap<>();
		Map<Tariff, String> deniedTariffsActual = new HashMap<>();
		Map<CallParameterName, Object> nativePriceTable = new HashMap<>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<>();
		Map<CallParameterName, Object> otherPriceTable = new HashMap<>();
		Tariff tariff = new CallTariff("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
										nativePriceTable, stationaryPriceTable, otherPriceTable);
		deniedTariffsExpected.put(tariff, "reason");	
		AddService.addDeniedTariff(deniedTariffsActual, "reason", "t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 2400, 
				nativePriceTable, stationaryPriceTable, otherPriceTable);
		Assert.assertEquals(deniedTariffsExpected, deniedTariffsActual);
    }
	
	/**
	 * Tests adding a denied call tariff
	 */
	@Test
    public void addDeniedInternetTariff() {
		Map<Tariff, String> deniedTariffsExpected = new HashMap<>();
		Map<Tariff, String> deniedTariffsActual = new HashMap<>();
		Map<CallParameterName, Object> nativePriceTable = new HashMap<>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<>();
		Map<CallParameterName, Object> otherPriceTable = new HashMap<>();
		Map<InternetParameterName, Object> inernetPriceTable = new HashMap<>();
		Tariff tariff = new InternetTariff("t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 15, 
										nativePriceTable, stationaryPriceTable, otherPriceTable, inernetPriceTable);
		deniedTariffsExpected.put(tariff, "reason");	
		AddService.addDeniedTariff(deniedTariffsActual, "reason", "t2012_no.1", "MTS", TariffName.OTLICHNIY, 54900, 30000, 7, 251, 750, 15, 
				nativePriceTable, stationaryPriceTable, otherPriceTable, inernetPriceTable);
		Assert.assertEquals(deniedTariffsExpected, deniedTariffsActual);
    }
}

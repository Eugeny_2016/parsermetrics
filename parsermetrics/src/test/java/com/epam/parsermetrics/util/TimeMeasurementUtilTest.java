package com.epam.parsermetrics.util;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

/**
 * The Class for testing methods of TimeMeasurmentUtil class
 */
public class TimeMeasurementUtilTest {
	
	/**
	 * Tests start time initialization
	 */
	@Test
    public void startMeasurement() {
		long seconds = 19721680134834L;
		TimeMeasurementUtil.startMeasurement(seconds);
		Assert.assertEquals(seconds, TimeMeasurementUtil.getStartTime());
    }
	
	/**
	 * Tests end time initialization and checks that results were written to the file
	 * @throws IOException 
	 */
	@Test
    public void endMeasurement() throws IOException {
		long seconds = 19721680134834L;
		TimeMeasurementUtil.endMeasurementAndLog(seconds, "result/result.txt");
		Assert.assertEquals(seconds, TimeMeasurementUtil.getEndTime());
	}
}

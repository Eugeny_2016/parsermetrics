package com.epam.parsermetrics.service.parsing.factory;

import com.epam.parsermetrics.exception.TariffException;
import com.epam.parsermetrics.service.parsing.TariffCollectionBuilder;
import com.epam.parsermetrics.service.parsing.dom.DOMTariffCollectionBuilder;
import com.epam.parsermetrics.service.parsing.sax.SAXTariffCollectionBuilder;
import com.epam.parsermetrics.service.parsing.stax.StAXTariffCollectionBuilder;

public class TariffCollectionBuilderFactory {
	
	public TariffCollectionBuilder getTariffBuilder(String parserType) throws TariffException {
		ParserType type = ParserType.valueOf(parserType.toUpperCase());
		switch (type) {
		case SAX:
			return new SAXTariffCollectionBuilder();
		case STAX:
			return new StAXTariffCollectionBuilder();
		case DOM:
			return new DOMTariffCollectionBuilder();
		default:
			throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
		}
	}
}

enum ParserType {
	SAX, STAX, DOM
}
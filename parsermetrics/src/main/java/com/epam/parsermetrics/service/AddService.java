package com.epam.parsermetrics.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.epam.parsermetrics.entity.CallParameterName;
import com.epam.parsermetrics.entity.CallTariff;
import com.epam.parsermetrics.entity.InternetParameterName;
import com.epam.parsermetrics.entity.InternetTariff;
import com.epam.parsermetrics.entity.Tariff;
import com.epam.parsermetrics.entity.TariffName;

/**
 * Class with static methods for adding tariff information into collections
 * @author Evgueni_Gordienko
 *
 */
public class AddService {
	/**
	 * Method for adding call tariffs
	 * @param tariffSet tariff set
	 * @param tariffId tariff id 
	 * @param operatorName operator name
	 * @param tariffName tariff name
	 * @param connectionFee connection fee
	 * @param initialPayment initial payment
	 * @param internetSpeedDefault Internet default speed
	 * @param smsPrice sms price
	 * @param mmsPrice mms price
	 * @param everyMegabytePrice every megabyte price
	 * @param nativePriceTable prices for native network
	 * @param stationaryPriceTable prices for stationary network
	 * @param otherNetworkPriceTable prices for other networks
	 * @return the result of add operation
	 */
	public static boolean addTariff(Set<Tariff> tariffSet, String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice,int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		Tariff tariff = new CallTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment,
									   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice,
									   new HashMap<CallParameterName, Object>(nativePriceTable), 
									   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
									   new HashMap<CallParameterName, Object>(otherNetworkPriceTable));
		return tariffSet.add(tariff);
	}

	/**
	 * Method for adding Internet tariffs
	 * @param tariffSet tariff set
	 * @param tariffId tariff id 
	 * @param operatorName operator name
	 * @param tariffName tariff name
	 * @param connectionFee connection fee
	 * @param initialPayment initial payment
	 * @param internetSpeedDefault Internet default speed
	 * @param smsPrice sms price
	 * @param mmsPrice mms price
	 * @param nativePriceTable prices for native network
	 * @param stationaryPriceTable prices for stationary network
	 * @param otherNetworkPriceTable prices for other networks
	 * @return the result of add operation
	 * @param internetPriceTable Internet prices
	 * @return the result of add operation
	 */
	public static boolean addTariff(Set<Tariff> tariffSet, String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
								   int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable, 
								   Map<InternetParameterName, Object> internetPriceTable) {
		
		Tariff tariff = new InternetTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
										   internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax,
										   new HashMap<CallParameterName, Object>(nativePriceTable), 
										   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
										   new HashMap<CallParameterName, Object>(otherNetworkPriceTable), 
										   new HashMap<InternetParameterName, Object>(internetPriceTable));
		return tariffSet.add(tariff);
	}

	/**
	 * Method for adding a denied call tariff
	 * @param deniedTariffs denied tariffs
	 * @param reason reason
	 * @param tariffId tariff id 
	 * @param operatorName operator name
	 * @param tariffName tariff name
	 * @param connectionFee connection fee
	 * @param initialPayment initial payment
	 * @param internetSpeedDefault Internet default speed
	 * @param smsPrice sms price
	 * @param mmsPrice mms price
	 * @param everyMegabytePrice every megabyte price
	 * @param nativePriceTable prices for native network
	 * @param stationaryPriceTable prices for stationary network
	 * @param otherNetworkPriceTable prices for other networks
	 */
	public static void addDeniedTariff(Map<Tariff, String> deniedTariffs, String reason, String tariffId, String operatorName, TariffName tariffName, 
										  int connectionFee, int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice,int everyMegabytePrice,  
										  Map<CallParameterName, Object> nativePriceTable, 
										  Map<CallParameterName, Object> stationaryPriceTable, 
										  Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		Tariff tariff = new CallTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment,
									   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice,
									   new HashMap<CallParameterName, Object>(nativePriceTable), 
									   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
									   new HashMap<CallParameterName, Object>(otherNetworkPriceTable));
		deniedTariffs.put(tariff, reason);
	}																												

	/**
	 * Method for adding a denied Internet tariff
	 * @param deniedTariffs denied tariffs
	 * @param reason reason
	 * @param tariffId tariff id 
	 * @param operatorName operator name
	 * @param tariffName tariff name
	 * @param connectionFee connection fee
	 * @param initialPayment initial payment
	 * @param internetSpeedDefault Internet default speed
	 * @param smsPrice sms price
	 * @param mmsPrice mms price
	 * @param internetSpeedMax Internet speed max
	 * @param nativePriceTable prices for native network
	 * @param stationaryPriceTable prices for stationary network
	 * @param otherNetworkPriceTable prices for other networks
	 * @param internetPriceTable Internet prices
	 */
	public static void addDeniedTariff(Map<Tariff, String> deniedTariffs, String reason, String tariffId, String operatorName, TariffName tariffName, 
										  int connectionFee, int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
										  Map<CallParameterName, Object> nativePriceTable, 
										  Map<CallParameterName, Object> stationaryPriceTable, 
										  Map<CallParameterName, Object> otherNetworkPriceTable, 
										  Map<InternetParameterName, Object> internetPriceTable) {
		
		Tariff tariff = new InternetTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
										   internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax,
										   new HashMap<CallParameterName, Object>(nativePriceTable), 
										   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
										   new HashMap<CallParameterName, Object>(otherNetworkPriceTable), 
										   new HashMap<InternetParameterName, Object>(internetPriceTable));
		deniedTariffs.put(tariff, reason);
	}
}

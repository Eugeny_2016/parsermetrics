package com.epam.parsermetrics.service.parsing.dom;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.epam.parsermetrics.entity.CallParameterName;
import com.epam.parsermetrics.entity.InternetParameterName;
import com.epam.parsermetrics.entity.TariffName;
import com.epam.parsermetrics.exception.TariffException;
import com.epam.parsermetrics.service.AddService;
import com.epam.parsermetrics.service.ValidationService;
import com.epam.parsermetrics.service.parsing.TariffCollectionBuilder;
import com.epam.parsermetrics.util.TimeMeasurementUtil;

/**
 * This is a builder implementation that builds tariffs set
 * after parsing process with DOM parser
 * @author Evgueni_Gordienko
 *
 */
public class DOMTariffCollectionBuilder extends TariffCollectionBuilder {
	
	/** Logger for the class */
	private static final Logger LOG = Logger.getLogger(DOMTariffCollectionBuilder.class);
	
	/** Parser itself */
	private DOMParser parser;
	
	/**
	 * The constructor
	 */
	public DOMTariffCollectionBuilder() {
		parser = new DOMParser();
	}
	
	@Override
	public void buildTariffSet(String fileName, String resultingFile) {
		LOG.info("DOM parsing process has started");
		tariffSet.clear();
		
		try {
			InputStream input = new FileInputStream(fileName);
			InputSource source = new InputSource(input);
			
			TimeMeasurementUtil.startMeasurement(System.currentTimeMillis());
			parser.parse(source);
		} catch (IOException ex) {
			LOG.error("Some I/O exception has occured while parsing with DOM-parser");
		} catch (SAXException ex) {
			LOG.error("Some SAX exception has occured while parsing with DOM-parser");
		}
		
		Document document = parser.getDocument();
		if (LOG.isDebugEnabled()) {
			LOG.debug("DOM parsing process has come to an end");
		}
		
		Element root = document.getDocumentElement();
		NodeList tariffNodes = root.getChildNodes();

		for(int i = 1; i < tariffNodes.getLength(); i+=2) {
			Element tariffElement = (Element) tariffNodes.item(i);
			try {
				buildTariff(tariffElement);
			} catch (TariffException ex) {
				LOG.error(ex.getMessage());
			}
		}
		TimeMeasurementUtil.endMeasurementAndLog(System.currentTimeMillis(), resultingFile);
		LOG.info("DOM parsing process has come to an end");
	}
	
	/**
	 * This method creates tariff object for every element of the DOM tree
	 * @param tariffElement tariff
	 * @throws TariffException
	 */
	private void buildTariff(Element tariffElement) throws TariffException {
		boolean insertResult = false;
		String value = null;
		String wrongTariffName = null;
		String negativeValidationReason = null;
		String tagName = tariffElement.getTagName();
				
		// variables that will contain the values of tariff element's attributes
		String tariffId = null;
		String operatorName = null;
		TariffName tariffName = null;
		int connectionFee = 0;
		int initialPayment = 0;
		int internetSpeedDefault = 0;
		Map<CallParameterName, Object> nativePriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> otherNetworkPriceTable = new HashMap<CallParameterName, Object>();
		int smsPrice = 0;
		int mmsPrice = 0;
		int everyMegabytePrice = 0;
		Map<InternetParameterName, Object> internetPriceTable = new HashMap<InternetParameterName, Object>();
		int internetSpeedMax = 0;	
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("tariff at present moment: " + tariffElement.getTagName() + " " + tariffElement.getAttribute("id"));
		}
		
		tariffId = tariffElement.getAttribute("id");
		operatorName = getTextContentOfChildElement(tariffElement, "operator-name");
		try {
			value = getTextContentOfChildElement(tariffElement, "tariff-name");
			tariffName = TariffName.valueOf(value.toUpperCase().replace(" ", "_"));
		} catch (IllegalArgumentException ex) {
			wrongTariffName = value;
			tariffName = null;
		}
		connectionFee = Integer.parseInt(getTextContentOfChildElement(tariffElement, "connection-fee"));
		initialPayment = Integer.parseInt(getTextContentOfChildElement(tariffElement, "initial-payment"));
		internetSpeedDefault = Integer.parseInt(getTextContentOfChildElement(tariffElement, "internet-speed-dafault"));
		nativePriceTable = getPriceTable(tariffElement, "native-network");
		stationaryPriceTable = getPriceTable(tariffElement, "stationary-network");
		otherNetworkPriceTable = getPriceTable(tariffElement, "other-network");
		smsPrice = Integer.parseInt(getTextContentOfChildElement(tariffElement, "sms"));
		mmsPrice = Integer.parseInt(getTextContentOfChildElement(tariffElement, "mms"));
		
		switch (tagName) {
		case "call-tariff":
			everyMegabytePrice = Integer.parseInt(getTextContentOfChildElement(tariffElement, "every-megabyte-price"));
			
			negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
														  		  internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
														  		  nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			if (negativeValidationReason == null) {
				insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
													internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
													nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
				if (LOG.isDebugEnabled()) {
					LOG.debug("Validation result is true!");
				}
				if (insertResult) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Insertion result is true!");
					}
					LOG.info("Tariff " + tariffName + " accepted!");
				} else {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Insertion result is false!");
					}
					LOG.info("Tariff " + tariffName + " denied!");
				}
				
			} else {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Validation result is false!");
				}
				if (wrongTariffName != null) {
					LOG.info("Tariff " + wrongTariffName + " denied!");
					tariffName = TariffName.UNKNOWN;
				} else {
					LOG.info("Tariff " + tariffName + " denied!");
				}
				AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			}
			break;
		case "internet-tariff":
			internetPriceTable = getPriceTable(tariffElement);
			internetSpeedMax = Integer.parseInt(getTextContentOfChildElement(tariffElement, "internet-speed-max"));
			
			negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
														  internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, nativePriceTable, 
														  stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
			if (negativeValidationReason == null) {
				insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
													internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, nativePriceTable, 
													stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
				if (LOG.isDebugEnabled()) {
					LOG.debug("Validation result is true!");
				}
				if (insertResult) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Insertion result is true!");
					}
					LOG.info("Tariff " + tariffName + " accepted!");
				} else {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Insertion result is false!");
					}
					LOG.info("Tariff " + tariffName + " denied!");
				}

			} else {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Validation result is false!\n");
				}
				if (wrongTariffName != null) {
					LOG.info("Tariff " + wrongTariffName + " denied!");
					tariffName = TariffName.UNKNOWN;
				} else {
					LOG.info("Tariff " + tariffName + " denied!");
				}
				AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			}
			break;
		default:
			throw new TariffException("Unknown xml-element was found in the element \"tariffs\"");
		}
	}
	
	/**
	 * Method for obtaining the contents of the child element 
	 * @param element parent element
	 * @param childName child name
	 * @return text content of the childs element
	 */
	private static String getTextContentOfChildElement(Element element, String childName) {
		String textContent = null;
		NodeList nodeList = element.getElementsByTagName(childName);
		textContent = nodeList.item(0).getTextContent().trim();;
		return textContent;
	}
	
	/**
	 * Method for obtaining the child element 
	 * @param element element
	 * @param childName child name
	 * @return child element
	 */
	private static Element getChildElement(Element element, String childName) {
		NodeList nodeList = element.getElementsByTagName(childName);
		Element childElement = (Element) nodeList.item(0);
		return childElement;
	}
	
	/**
	 * Method for obtaining phone call prices for the specific type of network
	 * @param tariffElement tariff element
	 * @param networkType network type
	 * @return map with prices per type of network
	 * @throws TariffException
	 */
	private Map<CallParameterName, Object> getPriceTable(Element tariffElement, String networkType) throws TariffException {
		Element element = null;
		NodeList nodeList = null;
		String textContent = null;
		String nodeName = null;
		Map<CallParameterName, Object> priceTable = new HashMap<CallParameterName, Object>();
		
		element = getChildElement(getChildElement(tariffElement, "call-prices"), networkType);
		nodeList = element.getChildNodes();
		
		for (int i = 1; i < nodeList.getLength(); i+=2) {
			nodeName = nodeList.item(i).getNodeName();
			textContent = getTextContentOfChildElement(element, nodeName);
			switch (nodeName) {
			case "minutes-without-discount":
				priceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(textContent));
				break;
			case "minutes-with-discount":
				priceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(textContent));
				break;
			case "discount-minute-price":
				priceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(textContent));
				break;
			case "common-minute-price":
				priceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(textContent));
				break;
			case "minute-type":
				priceTable.put(CallParameterName.MINUTE_TYPE, textContent);
				break;
			default:
				throw new TariffException("Unknown xml-element was found in the element \"call-prices\"");
			}
		}				
		return priceTable;
	}
	
	/**
	 * This is an overloaded version of getPriceTable() method for obtaining the Internet prices 
	 * @param tariffElement tariff element
	 * @return map with prices for the Internet
	 * @throws TariffException
	 */
	private Map<InternetParameterName, Object> getPriceTable(Element tariffElement) throws TariffException {
		Element element = null;
		NodeList nodeList = null;
		String textContent = null;
		String nodeName = null;
		Map<InternetParameterName, Object> priceTable = new HashMap<InternetParameterName, Object>();
		
		element = getChildElement(tariffElement, "mobile-internet");
		nodeList = element.getChildNodes();
		
		for (int i = 1; i < nodeList.getLength(); i+=2) {
			nodeName = nodeList.item(i).getNodeName();
			textContent = getTextContentOfChildElement(element, nodeName);
			switch (nodeName) {
			case "megabytes-with-discount":
				priceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.parseInt(textContent));
				break;
			case "discount-megabyte-price":
				priceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.parseInt(textContent));
				break;
			case "common-megabyte-price":
				priceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.parseInt(textContent));
				break;
			case "amount-of-traffic":
				priceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.parseInt(textContent));
				break;
			case "amount-of-fast-traffic":
				priceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.parseInt(textContent));
				break;
			case "megabyte-type":
				priceTable.put(InternetParameterName.MEGABYTE_TYPE, textContent);
				break;
			default:
				throw new TariffException("Unknown xml-element was found in the element \"mobile-internet\"");
			}
		}
		return priceTable;
	}
}

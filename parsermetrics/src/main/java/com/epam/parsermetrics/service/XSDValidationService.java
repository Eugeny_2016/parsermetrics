package com.epam.parsermetrics.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * Class for validating xml files
 * @author Evgueni_Gordienko
 *
 */
public class XSDValidationService {
	
	/** Logger for the class */
	private static final Logger LOG = Logger.getLogger(XSDValidationService.class);
	
	/**
	 * Method that validates xml file against its xsd schema 
	 * @param fileName file name
	 * @param schemaLocation schema location
	 * @return the result of validation
	 */
	public static boolean validateByXSD(String fileName, String schemaLocation) {
		boolean validationResult = false;
		
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		
		File schemaFile = new File(schemaLocation);												
		
		Schema schema = null;
		try {
			schema = factory.newSchema(schemaFile);												
		} catch (SAXException ex) {
			LOG.error("Couldn't open the schema file. " + ex.getMessage());
			return false;
		}
		
		Validator validator = schema.newValidator();											

		try {
			InputStream input = new FileInputStream(fileName);									
			Source source = new StreamSource(input);
			validator.validate(source);															
			validationResult = true;
			LOG.info("Document is valid!");
		} catch (SAXException | IOException ex) {
			LOG.error("Document is not valid because: " + ex.getMessage());
			return false;
		}
		return validationResult;
	}
}

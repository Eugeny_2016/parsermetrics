package com.epam.parsermetrics.service.parsing.stax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.epam.parsermetrics.entity.CallParameterName;
import com.epam.parsermetrics.entity.InternetParameterName;
import com.epam.parsermetrics.entity.TariffName;
import com.epam.parsermetrics.service.AddService;
import com.epam.parsermetrics.service.ValidationService;
import com.epam.parsermetrics.service.parsing.TariffCollectionBuilder;
import com.epam.parsermetrics.service.parsing.TariffsTagName;
import com.epam.parsermetrics.util.TimeMeasurementUtil;

/**
 * This is a builder implementation that builds tariffs set
 * after parsing process with SAX parser
 * @author Evgueni_Gordienko
 *
 */
@SuppressWarnings("incomplete-switch")
public class StAXTariffCollectionBuilder extends TariffCollectionBuilder {
	
	/** Logger for the class */
	private static final Logger LOG = Logger.getLogger(StAXTariffCollectionBuilder.class);
	
	/** Input factory */
	private XMLInputFactory inputFactory;
	
	/**
	 * The constructor
	 */
	public StAXTariffCollectionBuilder() {
		inputFactory = XMLInputFactory.newInstance();
	}
	
	@Override
	public void buildTariffSet(String fileName, String resultingFile) {
		InputStream input = null;
		XMLStreamReader reader = null;
		try {
			input = new FileInputStream(fileName);			
			reader = inputFactory.createXMLStreamReader(input);
			
			TimeMeasurementUtil.startMeasurement(System.currentTimeMillis());
			buildTariff(reader);
			TimeMeasurementUtil.endMeasurementAndLog(System.currentTimeMillis(), resultingFile);
			
			LOG.info("StAX parsing process has come to an end");
		} catch (FileNotFoundException ex) {
			LOG.error("File wasn't found while parsing with StAX-parser" + "\n" + ex);
		} catch (XMLStreamException ex) {
			LOG.error("Some XMLStream exception has occured while parsing with StAX-parser" + "\n" + ex);
		}		
	}
	
	private void buildTariff(XMLStreamReader reader) throws XMLStreamException {
		TariffsTagName tagName = null;
		int type = reader.getEventType();
		boolean nativeFlag = false;
		boolean stationaryFlag = false;
		boolean othernetFlag = false;
		boolean insertResult = false;
		String negativeValidationReason = null;
		String wrongTariffName = null;
		String reason = null;
		
		// variables that will contain the values per tariff from the parsed file 
		String tariffId = null;
		String operatorName = null;
		TariffName tariffName = null;
		int connectionFee = 0;
		int initialPayment = 0;
		int internetSpeedDefault = 0;
		int smsPrice = 0;
		int mmsPrice = 0;
		Map<CallParameterName, Object> nativePriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> otherNetworkPriceTable = new HashMap<CallParameterName, Object>();
		int everyMegabytePrice = 0;
		Map<InternetParameterName, Object> internetPriceTable = new HashMap<InternetParameterName, Object>();
		int internetSpeedMax = 0;
				
		while (reader.hasNext()) {
			try {
				switch (type) {
				case XMLStreamConstants.START_DOCUMENT:
					tariffSet.clear();
					LOG.info("StAX parsing process has started");
					break;
				case XMLStreamConstants.START_ELEMENT:
					tagName = TariffsTagName.valueOf(reader.getLocalName().toUpperCase().replace("-", "_"));
					switch (tagName) {
						case CALL_TARIFF:
						case INTERNET_TARIFF:
							tariffId = reader.getAttributeValue(null, "id");
							break;
						case NATIVE_NETWORK:
							nativeFlag = true;
							break;
						case STATIONARY_NETWORK:
							stationaryFlag = true;
							break;
						case OTHER_NETWORK:
							othernetFlag = true;
							break;
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					String text = reader.getText().trim();
					if(text.isEmpty()) {
						break;
					}
					switch (tagName) {
					case OPERATOR_NAME:
						operatorName = text;
						break;
					case TARIFF_NAME:
						try {
							tariffName = TariffName.valueOf(text.toUpperCase().replace(' ', '_'));
						} catch (IllegalArgumentException ex) {
							tariffName = TariffName.UNKNOWN;
							wrongTariffName = text.toString();
						}
						break;
					case CONNECTION_FEE:
						connectionFee = Integer.parseInt(text);
						break;
					case INITIAL_PAYMENT:
						initialPayment = Integer.parseInt(text);
						break;
					case INTERNET_SPEED_DAFAULT:
						internetSpeedDefault = Integer.parseInt(text);
						break;
					case INTERNET_SPEED_MAX:
						internetSpeedMax = Integer.parseInt(text);
						break;
					case EVERY_MEGABYTE_PRICE:
						everyMegabytePrice = Integer.parseInt(text);
						break;
					case SMS:
						smsPrice = Integer.parseInt(text);
						break;
					case MMS:
						mmsPrice = Integer.parseInt(text);
						break;
					case MINUTES_WITHOUT_DISCOUNT:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text));
						}
						break;
					case MINUTES_WITH_DISCOUNT:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text));
						}
						break;
					case DISCOUNT_MINUTE_PRICE:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text));
						}
						break;
					case COMMON_MINUTE_PRICE:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text));
						}
						break;
					case MINUTE_TYPE:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.MINUTE_TYPE, text);
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.MINUTE_TYPE, text );
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.MINUTE_TYPE, text);
						}
						break;
					case MEGABYTES_WITH_DISCOUNT:
						internetPriceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.parseInt(text));
						break;
					case DISCOUNT_MEGABYTE_PRICE:
						internetPriceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.parseInt(text));
						break;
					case COMMON_MEGABYTE_PRICE:
						internetPriceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.parseInt(text));
						break;
					case AMOUNT_OF_TRAFFIC:
						internetPriceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.parseInt(text));
						break;
					case AMOUNT_OF_FAST_TRAFFIC:
						internetPriceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.parseInt(text));
						break;
					case MEGABYTE_TYPE:
						internetPriceTable.put(InternetParameterName.MEGABYTE_TYPE, text);
						break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					tagName = TariffsTagName.valueOf(reader.getLocalName().toUpperCase().replace("-", "_"));
					switch (tagName) {
					case NATIVE_NETWORK:
						nativeFlag = false;
						break;
					case STATIONARY_NETWORK:
						stationaryFlag = false;
						break;
					case OTHER_NETWORK:
						othernetFlag = false;
						break;
					case CALL_TARIFF:
						if (tariffName == TariffName.UNKNOWN) {
							LOG.info("Tariff " + wrongTariffName + " denied!");
							reason = "wrong tariff name";
							AddService.addDeniedTariff(deniedTariffs, reason, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
												 	   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
												 	   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
							break;
						}
						negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																 internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
																 nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);

						if (negativeValidationReason == null) {
							insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
																nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
							if (LOG.isDebugEnabled()) {
								LOG.debug("Validation result is true!");
							}
							if (insertResult) {
								if (LOG.isDebugEnabled()) {
									LOG.debug("Insertion result is true!");
								}
								LOG.info("Tariff " + tariffName + " accepted!");
							} else {
								if (LOG.isDebugEnabled()) {
									LOG.debug("Insertion result is false!");
								}
								LOG.info("Tariff " + tariffName + " denied!");
							}
							
						} else {
							if (LOG.isDebugEnabled()) {
								LOG.debug("Validation result is false!");
							}
							LOG.info("Tariff " + tariffName + " denied!");
							AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 
													   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
													   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						}
						clearVariables(nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						break;
					case INTERNET_TARIFF:
						if (tariffName == TariffName.UNKNOWN) {
							LOG.info("Tariff " + wrongTariffName + " denied!");
							AddService.addDeniedTariff(deniedTariffs, reason, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
									 				   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
									 				   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
							break;
						}
						negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, 
																nativePriceTable, stationaryPriceTable, otherNetworkPriceTable,
																internetPriceTable);

						if (negativeValidationReason == null) {
							insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, 
																nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, 
																internetPriceTable);
							if (LOG.isDebugEnabled()) {
								LOG.debug("Validation result is true!");
							}
							if (insertResult) {
								if (LOG.isDebugEnabled()) {
									LOG.debug("Insertion result is true!");
								}
								LOG.info("Tariff " + tariffName + " accepted!");
							} else {
								if (LOG.isDebugEnabled()) {
									LOG.debug("Insertion result is false!\n");
								}
								LOG.info("Tariff " + tariffName + " denied!");
							}
							
						} else {
							if (LOG.isDebugEnabled()) {
								LOG.debug("Validation result is false!");
							}
							LOG.info("Tariff " + tariffName + " denied!");
							AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 
									   				   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
									   				   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						}
						clearVariables(nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
						break;
					}
					break;
				}
			} catch (NumberFormatException ex) {
				LOG.info("Incorrect content! Tariff id: " + tariffId + " xml-element: " + tagName);
			}
			type = reader.next();
		}
	}
	
	/**
	 * Methods that clears all maps with prices concerning call tariff 
	 * @param nativePriceTable
	 * @param stationaryPriceTable
	 * @param otherNetworkPriceTable
	 */
	private void clearVariables(Map<CallParameterName, Object> nativePriceTable, 
								Map<CallParameterName, Object> stationaryPriceTable, 
								Map<CallParameterName, Object> otherNetworkPriceTable) {
		nativePriceTable.clear();
		stationaryPriceTable.clear();
		otherNetworkPriceTable.clear();
	}
		
	/**
	 * Methods that clears all maps with prices concerning Internet tariff
	 * @param nativePriceTable
	 * @param stationaryPriceTable
	 * @param otherNetworkPriceTable
	 * @param internetPriceTable
	 */
	private void clearVariables(Map<CallParameterName, Object> nativePriceTable, 
								Map<CallParameterName, Object> stationaryPriceTable, 
								Map<CallParameterName, Object> otherNetworkPriceTable, 
								Map<InternetParameterName, Object> internetPriceTable) {
		nativePriceTable.clear();
		stationaryPriceTable.clear();
		otherNetworkPriceTable.clear();
		internetPriceTable.clear();
	}
}

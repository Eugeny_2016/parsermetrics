package com.epam.parsermetrics.service.parsing.sax;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.epam.parsermetrics.exception.TariffException;
import com.epam.parsermetrics.service.parsing.TariffCollectionBuilder;
import com.epam.parsermetrics.util.TimeMeasurementUtil;

/**
 * This is a builder implementation that builds tariffs set
 * after parsing process with StAX parser
 * @author Evgueni_Gordienko
 *
 */
public class SAXTariffCollectionBuilder extends TariffCollectionBuilder {
	
	/** Logger for the class */
	private static final Logger LOG = Logger.getLogger(SAXTariffCollectionBuilder.class);
	
	/** Handler for SAX parser */
	private SAXTariffHandler handler;
	
	/** Reader */
	private XMLReader reader;
		
	/**
	 * The constructor
	 * @throws TariffException
	 */
	public SAXTariffCollectionBuilder() throws TariffException {
		handler = new SAXTariffHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(handler);
			reader.setErrorHandler(handler);
		} catch (SAXException ex) {
			throw new TariffException("Error with getting reader-object for SAX parser", ex);
		}
	}
	
	/**
	 * Returns reader
	 * @return reader
	 */
	public XMLReader getReader() {
		return reader;
	}
	
	@Override
	public void buildTariffSet(String fileName, String resultingFile) {
		try {
			InputStream input = new FileInputStream(fileName);
			InputSource source = new InputSource(input);

			TimeMeasurementUtil.startMeasurement(System.currentTimeMillis());
			reader.parse(source);
			TimeMeasurementUtil.endMeasurementAndLog(System.currentTimeMillis(), resultingFile);
			
		} catch (IOException ex) {
			LOG.error("Some I/O exception has occured while parsing with SAX-parser" + "\n" + ex);
		} catch (SAXException ex) {
			LOG.error("Some SAX exception has occured while parsing with SAX-parser" + "\n" + ex);
		}
		tariffSet = handler.getTariffSet();
		deniedTariffs = handler.getDeniedTariffs();
	}
}

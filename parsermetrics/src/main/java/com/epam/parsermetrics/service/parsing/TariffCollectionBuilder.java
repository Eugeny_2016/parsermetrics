package com.epam.parsermetrics.service.parsing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.epam.parsermetrics.entity.Tariff;

/**
 * Abstract base class for all tariff collection builders 
 * @author Evgueni_Gordienko
 *
 */
public abstract class TariffCollectionBuilder {
	
	/** Accepted tariffs */
	protected Set<Tariff> tariffSet;
	
	/** Denied tariffs */
	protected Map<Tariff, String> deniedTariffs;
	
	/**
	 * The constructor
	 */
	public TariffCollectionBuilder() {
		tariffSet = new HashSet<Tariff>();
		deniedTariffs = new HashMap<Tariff, String>();
	}
	
	/**
	 * This method starts the parsing process
	 * @param fileName file name
	 * @param resultingFile resulting file
	 */
	public abstract void buildTariffSet(String fileName, String resultingFile);
	
	/**
	 * Returns accepted tariffs
	 * @return tariffSet
	 */
	public Set<Tariff> getTariffSet() {
		return tariffSet;
	}
	
	/**
	 * Returns denied tariffs
	 * @return deniedTariffs denied tariffs
	 */
	public Map<Tariff, String> getDeniedTariffs() {
		return deniedTariffs;
	}
}
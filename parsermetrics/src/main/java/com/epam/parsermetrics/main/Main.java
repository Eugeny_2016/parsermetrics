package com.epam.parsermetrics.main;

import com.epam.parsermetrics.command.Command;
import com.epam.parsermetrics.command.impl.ParseCommand;

public class Main {
	
	/** default amount of repeats */
	public static final int REPEAT_TIMES = 10;
	
	public static void main(String[] args) {
		Command command = new ParseCommand();
		
		if (args.length == 0) {
			command.execute(REPEAT_TIMES);
		}
		
		for (String arg : args) {
			try {
				int repeat = Integer.parseInt(arg);
				command.execute(repeat);
				break;
			} catch(NumberFormatException ex) {
				continue;
			}			
		}
	}
}

package com.epam.parsermetrics.entity;

/**
 * Enumeration of call parameters
 * @author Evgueni_Gordienko
 *
 */
public enum CallParameterName {
	MINUTES_WITHOUT_DISCOUNT,
	MINUTES_WITH_DISCOUNT,
	DISCOUNT_MINUTE_PRICE,
	COMMON_MINUTE_PRICE,
	MINUTE_TYPE;
}

package com.epam.parsermetrics.entity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Class that encapsulates tariff set
 * @author Evgueni_Gordienko
 *
 */
public class TariffSet {
	
	/** tariff set */
	private Set<Tariff> tariffSet;
	
	/**
	 * The constructor
	 */
	public TariffSet() {
		tariffSet = new HashSet<Tariff>();
	}
	
	public boolean addToTariffSet(Tariff tariff) {
		return tariffSet.add(tariff);
	}

	/**
	 * Method that returns a clone of tariff set 
	 * @return
	 */
	public Set<Tariff> getTariffSetClone() {
		Set<Tariff> clone = new HashSet<Tariff>();
		for (Tariff tariff : this.tariffSet) {
			clone.add(tariff);
		}
		return clone;
	}
	
	public Set<Tariff> getTariffSet() {
		return Collections.unmodifiableSet(tariffSet);
	}
}

package com.epam.parsermetrics.entity;

import java.util.Collections;
import java.util.Map;

/**
 * Class that represents only Internet tariffs
 * @author Evgueni_Gordienko
 *
 */
public class InternetTariff extends Tariff {
	
	/** Internet prices */
	private Map<InternetParameterName, Object> internetPriceTable;
	
	/** max Internet speed */
	private int internetSpeedMax;
	
	/**
	 * The constructor
	 */
	public InternetTariff() {
		super();
	}
	
	/**
	 * The constructor with arguments for initializing main fields
	 * @param tariffId tariff id
	 * @param operatorName operator name
	 * @param tariffName tariff name
	 * @param connectionFee connection fee
	 * @param initialPayment initial payment
	 * @param internetSpeedDefault Internet speed default
	 * @param smsPrice sms price
	 * @param mmsPrice mms price
	 * @param internetSpeedMax Internet speed max
	 * @param nativePriceTable prices for native network
	 * @param stationaryPriceTable prices for stationary network
	 * @param otherNetworkPriceTable prices for other networks
	 * @param internetPriceTable Internet prices
	 */
	public InternetTariff(String tariffId, String operatorName, TariffName tariffName, int connectionFee,
			int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
			Map<CallParameterName, Object> nativePriceTable,
			Map<CallParameterName, Object> stationaryPriceTable,
			Map<CallParameterName, Object> otherNetworkPriceTable,
			Map<InternetParameterName, Object> internetPriceTable) {
		
		super(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault,
				nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, smsPrice, mmsPrice);
		
		this.internetPriceTable = internetPriceTable;
		this.internetSpeedMax = internetSpeedMax;
		
	}
	
	public Map<InternetParameterName, Object> getInternetPriceTable() {
		return Collections.unmodifiableMap(internetPriceTable);
	}

	public int getInternetSpeedMax() {
		return internetSpeedMax;
	}

	public void setInternetPriceTable(Map<InternetParameterName, Object> internetPriceTable) {
		this.internetPriceTable = internetPriceTable;
	}
	
	public void setInternetSpeedMax(int internetSpeedMax) {
		this.internetSpeedMax = internetSpeedMax;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append(super.toString());
		str.append(" Internet prices: ").append(internetPriceTable);
		str.append(" Internet maximum speed: ").append(internetSpeedMax);
		
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return (int) (super.hashCode() + internetPriceTable.hashCode()*7 + internetSpeedMax*10);
	}

	@Override
	public boolean equals(Object obj) {
		InternetTariff internetTariff = (InternetTariff) obj;
		
		if (!super.equals(obj)) {
			return false;
		}
		
		if (!this.internetPriceTable.equals(internetTariff.internetPriceTable)) {
			return false;
		}
		
		if (internetSpeedMax != internetTariff.internetSpeedMax) {
			return false;
		}
		
		return true;
	}

}

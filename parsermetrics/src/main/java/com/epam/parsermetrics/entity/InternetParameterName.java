package com.epam.parsermetrics.entity;

/**
 * Enumeration of Internet parameters
 * @author Evgueni_Gordienko
 *
 */
public enum InternetParameterName {
	MEGABYTES_WITH_DISCOUNT,
	DISCOUNT_MEGABYTE_PRICE,
	COMMON_MEGABYTE_PRICE,
	AMOUNT_OF_TRAFFIC,
	AMOUNT_OF_FAST_TRAFFIC,
	MEGABYTE_TYPE;
}

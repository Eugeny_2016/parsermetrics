package com.epam.parsermetrics.entity;

/**
 * Enumeration of tariff names
 * @author Evgueni_Gordienko
 *
 */
public enum TariffName {
	OTLICHNIY, OBSHAISYA, LEGKO_SKAZAT, 
	SMART, SMART_MINI, SMART_PLUS, 
	INTERNET_MINI, INTERNET_MIDI, INTERNET_MAXI, 
	BIT, SUPER_BIT, VIP_BIT,
	UNIVERSALITY, UNIVERSALITY_PLUS, UNIVERSALITY_MAX, UNKNOWN
}

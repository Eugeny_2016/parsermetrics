package com.epam.parsermetrics.entity;

import java.util.Map;

/**
 * Class that represents only call tariffs
 * @author Evgueni_Gordienko
 *
 */
public class CallTariff extends Tariff {
	
	/** Price for every megabyte */
	private int everyMegabytePrice;
	
	/**
	 * The constructor with no arguments
	 */
	public CallTariff() {
		super();
	}
	
	/**
	 * The constructor with arguments for initializing main fields
	 * @param tariffId tariff id
	 * @param operatorName operator name
	 * @param tariffName tariff name
	 * @param connectionFee connection fee
	 * @param initialPayment initial payment
	 * @param internetSpeedDefault Internet speed default
	 * @param smsPrice sms price
	 * @param mmsPrice mms price
	 * @param everyMegabytePrice price for every megabyte
	 * @param nativePriceTable prices for native network
	 * @param stationaryPriceTable prices for stationary network
	 * @param otherNetworkPriceTable prices for other networks
	 */
	public CallTariff(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
					  int internetSpeedDefault,int smsPrice, int mmsPrice, int everyMegabytePrice, 
					  Map<CallParameterName, Object> nativePriceTable, 
					  Map<CallParameterName, Object> stationaryPriceTable, 
					  Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		super(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, 
				nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, smsPrice, mmsPrice);
		
		this.everyMegabytePrice = everyMegabytePrice;
	}
		
	public int getEveryMegabytePrice() {
		return everyMegabytePrice;
	}

	public void setEveryMegabytePrice(int everyMegabytePrice) {
		this.everyMegabytePrice = everyMegabytePrice;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append(super.toString());
		str.append(" Every megabyte price: ").append(everyMegabytePrice);
		
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return (int) (super.hashCode() + everyMegabytePrice*31);
	}

	@Override
	public boolean equals(Object obj) {
		CallTariff callTariff = (CallTariff) obj;
		
		if (!super.equals(obj)) {
			return false;
		}
					
		if (this.everyMegabytePrice != callTariff.everyMegabytePrice) {
			return false;
		}
		
		return true;
	}
}

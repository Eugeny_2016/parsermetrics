package com.epam.parsermetrics.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This represents an utilitarian class with methods for time measurement
 * @author Evgueni_Gordienko
 *
 */
public class TimeMeasurementUtil {

	/** The time when measurement started */
	private static long startTime;
	
	/** The time when measurement ended */
	private static long endTime;
	
	public static long getStartTime() {
		return startTime;
	}

	public static long getEndTime() {
		return endTime;
	}
	
	/**
	 * Initializes the startTime field
	 * @param seconds
	 */
	public static void startMeasurement(long seconds) {
		startTime = seconds;
	}
	
	/**
	 * Initializes the endTime field and writes the measurement into the resulting file
	 * @param seconds
	 */
	public static void endMeasurementAndLog(long seconds, String resultingFile) {
		endTime = seconds;
		try {
			File file = new File(resultingFile);

			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			
			pw.print("The elapsed time of parsing process is: " + (endTime - startTime) + " ms" + "\n");
			pw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
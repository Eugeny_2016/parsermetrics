package com.epam.parsermetrics.command;

/**
 * Command interface
 * @author Evgueni_Gordienko
 *
 */
public interface Command {
	/**
	 * Method that executes the logic of a command several times
	 * @param repeat Amount of repeats
	 */
	void execute(int repeat);
}

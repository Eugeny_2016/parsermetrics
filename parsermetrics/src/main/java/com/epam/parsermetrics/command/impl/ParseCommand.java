package com.epam.parsermetrics.command.impl;

import org.apache.log4j.Logger;

//import org.apache.log4j.Logger;
import com.epam.parsermetrics.command.Command;
import com.epam.parsermetrics.exception.TariffException;
import com.epam.parsermetrics.service.XSDValidationService;
import com.epam.parsermetrics.service.parsing.TariffCollectionBuilder;
import com.epam.parsermetrics.service.parsing.factory.TariffCollectionBuilderFactory;

public class ParseCommand implements Command {
	
	/** Logger for the class */
	private static final Logger LOG = Logger.getLogger(ParseCommand.class);
	
	/** xml source path */
	private static final String XML_SOURCE_PATH = "xml/tariffs.xml";
	
	/** xsd source path */
	private static final String XSD_SOURCE_PATH = "xml/tariffs.xsd";
	
	/** Resulting file path for SAX parser */
	private static final String RESULT_FILE_PATH_SAX = "result/resultSax.txt";
	
	/** Resulting file path for StAX parser */
	private static final String RESULT_FILE_PATH_STAX = "result/resultStax.txt";
	
	/** Resulting file path for DOM parser */
	private static final String RESULT_FILE_PATH_DOM = "result/resultsDom.txt";
	
	/** SAX parser constant */
	private static final String SAX_PARSER = "sax";
	
	/** StAX parser constant */
	private static final String STAX_PARSER = "stax";
	
	/** DOM parser constant */
	private static final String DOM_PARSER = "dom";
	
	@Override
	public void execute(int repeat) {
		TariffCollectionBuilder builder = null;
		TariffCollectionBuilderFactory factory = null;
			
		// If xml file is valid start parsing process
		if (XSDValidationService.validateByXSD(XML_SOURCE_PATH, XSD_SOURCE_PATH)) {
			try {
				factory = new TariffCollectionBuilderFactory();
				
				builder = factory.getTariffBuilder(SAX_PARSER);
				build(builder, repeat, RESULT_FILE_PATH_SAX);
				
				builder = factory.getTariffBuilder(STAX_PARSER);
				build(builder, repeat, RESULT_FILE_PATH_STAX);
				
				builder = factory.getTariffBuilder(DOM_PARSER);
				build(builder, repeat, RESULT_FILE_PATH_DOM);		
				
			} catch (TariffException ex) {
				throw new RuntimeException(ex);
			}
		} else {
			throw new RuntimeException("Documentation is not valid!");
		}		
	}
	
	private void build(TariffCollectionBuilder builder, int repeat, String resultingFile) {
		for (int i = 0; i < repeat; i++) {
			builder.buildTariffSet(XML_SOURCE_PATH, resultingFile);
		}
	}
}
